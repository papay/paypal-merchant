<?php

return [
    'id' => 'merchant-console',
    'bootstrap' => ['log', 'gii'],
    'basePath' => __DIR__ . '/..',
    'language' => 'ru',
    'timeZone' => 'Europe/Kiev',
    'controllerNamespace' => 'yii\console\controllers',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'charset' => 'utf-8',
        'log' => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
];