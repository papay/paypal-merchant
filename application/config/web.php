<?php

return [
    'id' => 'merchant',
    'basePath' => __DIR__ . '/..',
    'language' => 'ru',
    'timeZone' => 'Europe/Kiev',
    'vendorPath' => '@app/../vendor',
    'components' => [
        'request' => [
            'class' => 'yii\web\Request',
            'cookieValidationKey' => 'sYy9nbcs8SgqYGdV6ZnWjBAh',
            'enableCsrfValidation' => false,
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\User',
        ],
        'urlManager' => require(__DIR__ . '/routes.php'),
        'charset' => 'utf-8',
        'db' => require(__DIR__ . '/db.php'),
        'paypal'=> [
            'class'        => 'app\components\Paypal',
            'clientId'     => 'AfhbTJxdhJgY5zQlwL1bsiawp9qoHhpqPFfAPp8zIXAFgneFWdqT-D0PMQ9YqY_u9kWb8DSSehG9kT_X',
            'clientSecret' => 'EIPy85YbJEarE9BaidbLzlquCZk9mTbK32mbfiyR5BBSuvq1zSwN8qSnFjpiK8JQzveVZRoRmNnu7x78',
            'isProduction' => false,
        ],
    ],
    'on beforeRequest' => function () {
        $pathInfo = Yii::$app->request->pathInfo;
        $query = Yii::$app->request->queryString;
        if (!empty($pathInfo) && substr($pathInfo, -1) !== '/') {
            $url = '/' . $pathInfo . '/';
            if ($query) {
                $url .= '?' . $query;
            }
            Yii::$app->response->redirect($url, 301);
            Yii::$app->end();
        }
    },
];