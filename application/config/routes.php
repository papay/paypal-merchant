<?php

return [
    'enablePrettyUrl'     => true,
    'showScriptName'      => false,
    'enableStrictParsing' => true,
    'suffix'              => '/',
    'rules' => [
        '/' => 'index/index',
        [
            'pattern' => '<_c:[\w-]+>/<_a:[\w-]+>',
            'route' => '<_c>/<_a>',
            'defaults' => [
                '_c' => 'index',
                '_a' => 'index'
            ]
        ],
    ]
];