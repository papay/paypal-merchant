<?php

use yii\helpers\Url;
use yii\bootstrap\Html;

?>
<div class="container">
    <div class="row">
        <div class="text-center">
            <div class="jumbotron">
                <h1>Добро пожаловать!</h1>
                <p>Мы рады представить Вам наш новый zip-архив.</p>
                <p>Только сегодня действует специальная цена - $1.</p>
            </div>
        </div>

        <?php if (Yii::$app->user->isGuest) : ?>
            <div class="well text-center">
                <?=Html::a('Авторизируйтесь', Url::toRoute('user/auth'), [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal'
                ])?>, чтоб оплатить и скачать архив.
            </div>
        <?php else : ?>
            <?php
            $user = Yii::$app->user->getIdentity();
            if ($user->paid) : ?>
                <p class="text-center">Вы можете <a href="/user/download/">скачать архив</a>.</p>
            <?php else : ?>
                <p class="text-center">
                    <a href="<?=Url::toRoute(['user/pay'])?>">
                        <img src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/gold-pill-paypal-60px.png" alt="PayPal">
                    </a>
                </p>
            <?php endif; ?>
        <?php endif; ?>

    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>