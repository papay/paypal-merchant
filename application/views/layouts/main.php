<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/**
 * @var \yii\web\View $this
 */

$this->beginPage();

?>
<!doctype html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <?= Html::csrfMetaTags(); ?>
        <title><?= Html::encode($this->title . ' - Merchant'); ?></title>
        <?php $this->head(); ?>
    </head>
    <body>
        <?php $this->beginBody(); ?>
        <header>
            <?php

            NavBar::begin([
                'brandLabel' => 'Merchant',
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top'
                ]
            ]);
                echo Nav::widget([
                    'items' => [
                        ['label' => 'Войти', 'url' => ['user/auth'], 'linkOptions' => [
                            'data-toggle' => 'modal',
                            'data-target' => '#modal'
                        ], 'visible' => Yii::$app->user->isGuest],
                        ['label' => 'Выйти', 'url' => ['user/logout'], 'visible' => !Yii::$app->user->isGuest],
                    ],
                    'options' => ['class' => 'navbar-nav navbar-right'],
                ]);
            NavBar::end();

            ?>
        </header>
        <main style="padding-top: 80px">
            <?= $content; ?>
        </main>
        <footer></footer>

        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>