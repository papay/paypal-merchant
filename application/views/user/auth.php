<?php

use \yii\bootstrap\Html;
use \yii\bootstrap\ActiveForm;

?>
<div id="forms" class="carousel slide">
    <div class="carousel-inner">
        <div class="item active">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Авторизация</h4>
            </div>
            <div class="modal-body">
                <?php

                $form = ActiveForm::begin([
                    'id' => 'auth-form',
                    'action' => 'user/auth/',
                    'layout' => 'horizontal',
                    'enableAjaxValidation' => true,
                    'validationUrl' => '/user/auth/',
                ]);

                echo $form->field($model, 'username');
                echo $form->field($model, 'password')->passwordInput();
                ?>

                <p class="text-center">Если у Вас нет аккаунта, Вы можете <?=Html::a('зарегистрироваться', '#', [
                        'data-target' => '#forms',
                        'data-slide-to' => "1",
                    ])?></p>

                <hr/>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Войти</button>
                </div>
                <div class="clearfix"></div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="item">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Регистрация</h4>
            </div>
            <div class="modal-body">
                <?php

                $form = ActiveForm::begin([
                    'id' => 'reg-form',
                    'action' => 'user/register/',
                    'layout' => 'horizontal',
                    'enableAjaxValidation' => true,
                    'validationUrl' => '/user/register-validate/',
                ]);

                echo $form->field($model, 'username');
                echo $form->field($model, 'password')->passwordInput();
                ?>

                <p class="text-center">Если у Вас уже есть аккаунт, Вы можете <?=Html::a('авторизироваться', '#', [
                        'data-target' => '#forms',
                        'data-slide-to' => "0",
                    ])?></p>

                <hr/>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary" name="reg" value="1">Зарегистрироваться</button>
                </div>
                <div class="clearfix"></div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>