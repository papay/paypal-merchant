<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m161206_182524_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(30),
            'password' => $this->string(60),
            'paid' => $this->boolean()->defaultValue(0),
            'paymentId' => $this->string(30),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
