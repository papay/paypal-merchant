<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\bootstrap\ActiveForm;
use app\models\User;
use app\components\Paypal;

class UserController extends Controller
{
    /**
     * @var Paypal
     */
    protected $paypal;

    public function init()
    {
        $this->paypal = \Yii::$app->paypal;
    }

    public function actionAuth()
    {
        $result = [];

        if (!\Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }
        $model = new User();
        $model->setScenario('login');

        if (\Yii::$app->request->isPost) {

            $userData = \Yii::$app->request->post('User');
            $identity = User::findOne(['username' => $userData['username']]);

            if ($identity) {
                if (!empty($userData['password'])) {
                    if (\Yii::$app->security->validatePassword($userData['password'], $identity->password)) {
                        \Yii::$app->user->login($identity);
                        return $this->redirect('/');
                    } else {
                        $result['user-password'] = ['Неверный пароль'];
                    }
                } else {
                    $result['user-password'] = ['Неверный пароль'];
                }
            } else {
                $result['user-username'] = ['Проверьте правильность логина'];
            }

            if (\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }
        }

        if (\Yii::$app->request->isAjax) {
            echo $this->renderAjax('auth', [
                'model' => $model
            ]);
        } else {
            echo $this->render('auth', [
                'model' => $model
            ]);
        }
    }

    public function actionRegister()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }
        $model = new User();
        $model->setScenario('register');

        if (\Yii::$app->request->isPost) {

            $userData = \Yii::$app->request->post('User');
            $model->setAttributes($userData);

            if (\Yii::$app->request->post('reg', false)) {
                if ($model->save()) {
                    \Yii::$app->user->login($model);
                }
            }
        }
        return $this->redirect('/');
    }

    public function actionRegisterValidate()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (\Yii::$app->request->isAjax) {

            if (\Yii::$app->user->isGuest) {
                $model = new User();
                $model->setScenario('register');
                $model->load(\Yii::$app->request->post());
                return ActiveForm::validate($model);
            }
        }
        return [];
    }

    public function actionLogout()
    {
        if (!\Yii::$app->user->isGuest) {
            \Yii::$app->user->logout();
        }
        return $this->redirect('/');
    }

    public function actionPay()
    {
        if (!Yii::$app->user->isGuest) {
            try {
                /*
                 * make payment transaction
                 */
                $payment = $this->paypal->makePayment();

                if ($payment) {
                    return $this->redirect($payment->getApprovalLink());
                }
            } catch (\Exception $e) {}
        }
        return $this->redirect('/');
    }

    public function actionSuccess($paymentId, $token, $PayerID)
    {
        $this->view->title = 'Поздравляем!';

        if (!Yii::$app->user->isGuest) {
            try {
                /*
                 * Process and approve payment.
                 * And save payment information to user attributes.
                 */
                $payment = $this->paypal->processPayment($paymentId, $PayerID);
                if ($payment->state == 'approved') {
                    /**
                     * @var User $user
                     */
                    $user = \Yii::$app->user->getIdentity();
                    $user->paid = 1;
                    $user->paymentId = $paymentId;
                    $user->save();
                    return $this->render('success', []);
                }
            } catch (\Exception $e) {}
        }
        return $this->redirect('/');
    }

    public function actionFail()
    {
        $this->view->title = 'Ошибка';
        return $this->render('/index/index');
    }

    public function actionDownload()
    {
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->getIdentity();
            if ($user->paid) {
                $path = \Yii::getAlias('@app/../files/file.zip');
                $file = realpath($path);
                if (file_exists($file)) {
                    return \Yii::$app->response->sendFile($file);
                }
            }
        }
        return $this->redirect('/');
    }
}