<?php

namespace app\controllers;

use yii\web\Controller;

class IndexController extends Controller
{
    public function actionIndex()
    {
        $this->view->title = 'Главная страница';
        echo $this->render('index');
    }
}